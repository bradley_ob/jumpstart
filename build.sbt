name := "jumpStart"
 
version := "1.0" 
      
lazy val `jumpstart` = (project in file(".")).enablePlugins(PlayScala)

resolvers += "scalaz-bintray" at "https://dl.bintray.com/scalaz/releases"
      
resolvers += "Akka Snapshot Repository" at "https://repo.akka.io/snapshots/"
      
scalaVersion := "2.13.0"

libraryDependencies ++= Seq( ehcache , ws , specs2 % Test , guice )

libraryDependencies ++= Seq(
  "com.typesafe.play" %% "play-slick" % "4.0.2",
  "com.typesafe.play" %% "play-slick-evolutions" % "4.0.2",
  "org.postgresql" % "postgresql" % "42.2.5",
  "com.github.t3hnar" %% "scala-bcrypt" % "4.1",
  "javax.mail" % "javax.mail-api" % "1.6.2"
)

enablePlugins(DockerPlugin)
//unmanagedResourceDirectories in Test <+=  baseDirectory ( _ /"target/web/public/test" )
