-- !Ups

-- Users schema

CREATE TABLE users (
    id BIGSERIAL,
    name VARCHAR(255) NOT NULL,
    password VARCHAR(255) NOT NULL,
    email VARCHAR(255) NOT NULL,
    phone VARCHAR(255) NOT NULL,
    enabled BOOLEAN NOT NULL DEFAULT TRUE,
    registered_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
);

-- Events schema

CREATE TABLE events (
    id BIGSERIAL,
    title VARCHAR(255) NOT NULL,
    created_by BIGINT NOT NULL REFERENCES users(id),
    start_date TIMESTAMP NOT NULL,
    end_date TIMESTAMP,
    recurrence_type VARCHAR(255),
    description TEXT,
    PRIMARY KEY (id)
);

-- Response schema

CREATE TABLE responses (
    id BIGSERIAL,
    event_id BIGINT REFERENCES events(id),
    committed BOOLEAN NOT NULL,
    response_date TIMESTAMP NOT NULL,
    description TEXT,
    PRIMARY KEY (id)
);

-- !Downs
DROP TABLE users;
DROP TABLE events;
DROP TABLE responses;

