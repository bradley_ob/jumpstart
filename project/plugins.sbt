logLevel := Level.Warn

resolvers += "Typesafe repository" at "https://repo.typesafe.com/typesafe/releases/"

addSbtPlugin("com.typesafe.play" % "sbt-plugin" % "2.7.3")
addSbtPlugin("com.typesafe.sbt" % "sbt-play-ebean" % "5.0.2")
addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager" % "1.3.25")