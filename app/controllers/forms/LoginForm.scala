package controllers.forms

case class AuthData (email: String, password: String)

object LoginForm {
  import play.api.data.Form
  import play.api.data.Forms._

  val form = Form(
    mapping(
      "email" -> nonEmptyText,
      "password" -> nonEmptyText,
    )(AuthData.apply)(AuthData.unapply)
  )
}
