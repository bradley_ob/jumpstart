package controllers.forms

import java.util.Date

case class ResponseData (committed: Boolean, description: String)
object ResponseForm {
  import play.api.data.Form
  import play.api.data.Forms._
  import play.api.data.format._

  val dateTimeLocal: Formatter[Date] = Formats.dateFormat("yyyy-MM-dd'T'HH:mm")
  val form = Form(
    mapping(
      "committed" -> boolean,
      "description" -> text,
    )(ResponseData.apply)(ResponseData.unapply)
  )
}