package controllers.forms

case class UserData (
                     name: String,
                     email: String,
                     password: String,
                     phone: String
                     )

object UserForm {
  import play.api.data.Form
  import play.api.data.Forms._

  val form = Form(
    mapping(
     "name" -> nonEmptyText,
      "email" -> nonEmptyText,
      "password" -> nonEmptyText,
      "phone" -> nonEmptyText
    )(UserData.apply)(UserData.unapply)
  )
}
