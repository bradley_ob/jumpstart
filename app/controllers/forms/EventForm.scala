package controllers.forms
import java.util.Date

case class EventData (
                     title: String,
                     description: String,
                     startDate: Date,
                     endDate: Date,
                     recurrenceScheme: String
                     )
object EventForm {
  import play.api.data.Form
  import play.api.data.Forms._
  import play.api.data.format._

  val dateTimeLocal: Formatter[Date] = Formats.dateFormat("yyyy-MM-dd'T'HH:mm")
  val form = Form(
    mapping(
      "title" -> nonEmptyText,
      "description" -> text,
      "startDate" -> of(dateTimeLocal),
      "endDate" -> of(dateTimeLocal),
      "recurrenceScheme" -> nonEmptyText,
    )(EventData.apply)(EventData.unapply)
  )
}