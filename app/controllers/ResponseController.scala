package controllers

import controllers.actions.AuthenticatedUserAction
import javax.inject._
import models.PaginatedResult
import models.dao.EventDAO
import org.joda.time.DateTime
import play.api.mvc._
import services.{EventService, ResponseService, UserSessionService}
import views.html

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}

@Singleton
class ResponseController @Inject()(cc: ControllerComponents, authenticatedUserAction: AuthenticatedUserAction)(events: EventService, eventDao: EventDAO, userSessionService: UserSessionService, responses: ResponseService) extends AbstractController(cc) with play.api.i18n.I18nSupport {
  import forms.{ResponseData, ResponseForm}

  def respondForm(eventId: Long): Action[AnyContent] = authenticatedUserAction { implicit request: Request[AnyContent] =>
    Ok(views.html.respond(ResponseForm.form, eventId))
  }

  def respond(eventId: Long):  Action[AnyContent] = authenticatedUserAction.async { implicit request: Request[AnyContent] =>
    ResponseForm.form.bindFromRequest.fold(
      formWithErrors => {
        Future.successful(BadRequest(views.html.respond(formWithErrors, eventId)))
      },
      responseData => {
        for {
          _ <- responses.add(responseData, userSessionService.userId, eventId)
        } yield Redirect(routes.ResponseController.getResponses(eventId))
      }
    )
  }

  def getResponses(eventId: Long, page: Int): Action[AnyContent] = authenticatedUserAction.async { implicit request: Request[AnyContent] =>
    responses.getByEvent(eventId, page) map {
      response => Ok(views.html.responses(response, eventId))
    }
  }
}
