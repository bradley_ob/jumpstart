package controllers

import com.github.t3hnar.bcrypt._
import controllers.actions.AuthenticatedUserAction
import controllers.forms.{LoginForm, UserForm}
import javax.inject._
import models.User
import models.dao.UserDAO
import play.api.i18n.I18nSupport
import play.api.mvc._
import services.UserService

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{ExecutionContext, Future}

class UserController @Inject()(cc: ControllerComponents, userDao: UserDAO, authenticatedUserAction: AuthenticatedUserAction, userService: UserService) extends AbstractController(cc) with I18nSupport {
  val Home: Result = Redirect(routes.HomeController.index())
  def index(): Action[AnyContent] = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.signup(UserForm.form))
  }

  def addUser(): Action[AnyContent] = Action.async { implicit request: Request[AnyContent] =>
    UserForm.form.bindFromRequest.fold(
      formWithErrors => Future.successful(BadRequest(views.html.signup(formWithErrors))),
      user => {
        val userReq = User(None, user.name, user.email, user.password.bcrypt, user.phone)
        for {
          _ <- userDao.insert(userReq)
        } yield Home.flashing("success" -> "You have successfully signed up, %s".format(user.name))
      }
    )
  }

  def showLogin(): Action[AnyContent] = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.login(LoginForm.form))
  }

  def login(): Action[AnyContent] = Action.async { implicit request: Request[AnyContent] =>
    LoginForm.form.bindFromRequest.fold(
      formWithErrors => Future.successful(BadRequest(views.html.login(formWithErrors))),
      authData => userService.login(authData).map{
        case None => BadRequest(views.html.login(LoginForm.form)).flashing("bad_auth" -> "Incorrect username or password")
        case Some(user) => Ok(views.html.index("Welcome %s".format(user.name))) withSession("user" -> user.id.getOrElse(None).toString)
      }
    )
  }

  def logout(): Action[AnyContent] = authenticatedUserAction { implicit request: Request[AnyContent] =>
   request.session.get("user") match {
       case None => BadRequest("No user")
       case Some(_) => Ok(views.html.index("Logged out")).withNewSession
   }
  }
}
