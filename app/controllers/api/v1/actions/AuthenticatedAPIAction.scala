package controllers.api.v1.actions

import javax.inject.Inject
import play.api.mvc.Results.Forbidden
import play.api.mvc.{ActionBuilderImpl, BodyParsers, Request, Result}

import scala.concurrent.{ExecutionContext, Future}


class AuthenticatedAPIAction@Inject() (parser: BodyParsers.Default) (implicit executionContext: ExecutionContext)
  extends ActionBuilderImpl(parser) {
  override def invokeBlock[A](request: Request[A], block: Request[A] => Future[Result]): Future[Result] = {
    request.headers.get("auth") match {
      case None => Future.successful(Forbidden(views.html.index("Unauthorized request")))
      case Some(_) => block(request)
    }
  }
}
