package controllers

import controllers.actions.AuthenticatedUserAction
import javax.inject._
import models.PaginatedResult
import models.dao.EventDAO
import org.joda.time.DateTime
import play.api.mvc._
import services.{EventService, UserSessionService}
import views.html

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{ExecutionContext, Future}

@Singleton
class EventController @Inject()(cc: ControllerComponents, authenticatedUserAction: AuthenticatedUserAction)(events: EventService, eventDao: EventDAO, userSessionService: UserSessionService) extends AbstractController(cc) with play.api.i18n.I18nSupport {
  import forms.{EventData, EventForm}

  def createEventForm(): Action[AnyContent] = authenticatedUserAction { implicit request: Request[AnyContent] =>
    Ok(views.html.events(eventForm = EventForm.form))
  }

  def postForm(): Action[AnyContent] = authenticatedUserAction.async { implicit request: Request[AnyContent] =>
    EventForm.form.bindFromRequest.fold(
      formWithErrors => {
        Future.successful(BadRequest(views.html.events(formWithErrors)))
      },
      event => {
        for {
          _ <- events.add(event, userSessionService.userId)
        } yield
       Redirect(routes.EventController.listUserEvents(0))
      }
    )
  }

  def listUserEvents(page: Int): Action[AnyContent] = authenticatedUserAction.async { implicit request: Request[AnyContent] =>
    request.session.get("user") match {
      case Some(id) =>
        val idNum: Long = id.toLong
        val events: Future[PaginatedResult[models.Event]] = eventDao.getByUser(idNum, page, 20)
        events map { es => Ok(html.eventsList(es))}
      case None => Future.successful(Forbidden("I somehow lost your token?!"))
    }
  }

  def deleteEvent(id: Long): Action[AnyContent] = authenticatedUserAction.async { implicit request: Request[AnyContent] =>
    events.get(id).map {
      case Some(event) => {
        if (event.createdBy.!=(userSessionService.userId))  {
          Forbidden("You do not have permission to access this Event")
        } else {
          eventDao.delete(id)
          Redirect(routes.EventController.listUserEvents(0)).flashing("deleted" -> "Deleted event %l".format(id))
        }
      }
     case None => NotFound("No such Event")
    }
  }

  def editEventForm(id: Long):  Action[AnyContent] = authenticatedUserAction.async { implicit request: Request[AnyContent] =>
    events.get(id).map {
      case Some(event) => Ok(views.html.editEvent(
        EventForm.form.fill(
          EventData(
            event.title,
            event.description.getOrElse(""),
            event.startDate.getOrElse(new DateTime(0)).toDate,
            event.endDate.getOrElse(new DateTime(0)).toDate,
            event.recurrenceType.getOrElse("")
          )
        ), id)
      )
      case None => NotFound("Event does not exist")
    }
  }

  def editEvent(id: Long): Action[AnyContent] = authenticatedUserAction.async { implicit request: Request[AnyContent] =>
    EventForm.form.bindFromRequest.fold(
      formWithErrors => Future.successful(BadRequest(views.html.editEvent(formWithErrors, id))),
      eventData => {
        events update(id, request.session.get("user") match {case Some(id) => id.toLong case None => -69}, eventData)
        Future.successful(Redirect(routes.EventController.listUserEvents(0)))
      }
    )
  }
}
