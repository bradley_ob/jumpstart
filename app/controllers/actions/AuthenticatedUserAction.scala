package controllers.actions

import javax.inject.Inject
import play.api.mvc.Results._
import play.api.mvc._

import scala.concurrent.{ExecutionContext, Future}

class AuthenticatedUserAction @Inject() (parser: BodyParsers.Default) (implicit executionContext: ExecutionContext)
  extends ActionBuilderImpl(parser) {
    override def invokeBlock[A](request: Request[A], block: Request[A] => Future[Result]): Future[Result] = {
       request.session.get("user") match {
           case None => Future.successful(Forbidden(views.html.index("Unauthorized Request")))
           case Some(_) => block(request)
       }
    }
}
