package models.dao

import java.sql.Timestamp

import org.joda.time.DateTime
import play.api.db.slick.HasDatabaseConfigProvider
import slick.jdbc.JdbcProfile

trait BaseComponent { self: HasDatabaseConfigProvider[JdbcProfile] =>
  import profile.api._

  implicit val dateTime =
    MappedColumnType.base[DateTime, Timestamp](
      dt => new Timestamp(dt.getMillis),
      ts => new DateTime(ts.getTime)
  )
}

