package models.dao

import javax.inject.Inject
import models.{Event, PaginatedResult}
import org.joda.time.DateTime
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.jdbc.JdbcProfile
import slick.lifted.ProvenShape

import scala.concurrent.{ExecutionContext, Future}

trait EventsComponent extends BaseComponent { self: HasDatabaseConfigProvider[JdbcProfile] =>
  import profile.api._

  class EventsTable(tag: Tag) extends Table[Event](tag,"events") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def title = column[String]("title")
    def createdBy = column[Long]("created_by")
    def startDate = column[Option[DateTime]]("start_date")
    def endDate = column[Option[DateTime]]("end_date")
    def recurrenceType = column[Option[String]]("recurrence_type")
    def description = column[Option[String]]("description")
    def * : ProvenShape[Event] = (id.?, title, createdBy, startDate, endDate, recurrenceType, description) <> (Event.tupled, Event.unapply)
  }
}

class EventDAO @Inject()
(protected val dbConfigProvider: DatabaseConfigProvider)
(implicit executionContext: ExecutionContext)
extends EventsComponent with HasDatabaseConfigProvider[JdbcProfile]
{
  import profile.api._

  private val eventsTable = TableQuery[EventsTable]
  private val insertQuery = eventsTable returning eventsTable.map(_.id) into ((event, id) => event.copy(id = Some(id)))

  def getById(id: Long): Future[Option[Event]] = db.run(eventsTable.filter(_.id === id).result.headOption)

  def getByUser(userId: Long, page: Int = 0, pageSize: Int = 10): Future[PaginatedResult[Event]] = {
    val offset = page * pageSize
    for {
      usersEvents <- db.run(eventsTable.filter(_.createdBy === userId)
          .drop(offset).take(pageSize)
          .result)
      eventCount <- db.run(eventsTable.filter(_.createdBy === userId).length.result)
    } yield PaginatedResult(usersEvents, page, offset, eventCount)
  }

  def list(page: Int = 0, pageSize: Int = 10): Future[PaginatedResult[Event]] =  {
    val offset = page * pageSize
    for {
      allEvents <- db.run(eventsTable.drop(offset).take(pageSize).result)
      eventCount <- db.run(eventsTable.length.result)
    } yield PaginatedResult(allEvents, page, offset, eventCount)
  }

  def update(event: Event): Future[Option[Event]] = {
    val updatedEvent = event.copy(Some(event.id.get))
    db.run(eventsTable.filter(_.id === event.id).update(updatedEvent)).map{
      case 0 => None
      case _ => Some(event)
    }
  }
  def insert(event: Event): Future[Event] = {
    db run (insertQuery += event)
  }

  def insert(events: Seq[Event]): Future[Seq[Event]] = {
    db run (insertQuery ++= events)
  }

  def delete(id: Long): Future[Unit] = {
    db.run(eventsTable.filter(_.id === id).delete).map(_ => ())
  }
}
