package models.dao

import javax.inject.Inject
import models.{PaginatedResult, User}
import org.joda.time.DateTime
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.jdbc.JdbcProfile
import slick.jdbc.PostgresProfile.api._
import slick.lifted.ProvenShape

import scala.concurrent.{ExecutionContext, Future}

trait UsersComponent extends BaseComponent { self: HasDatabaseConfigProvider[JdbcProfile] =>

  class UsersTable(tag: Tag) extends Table[User](tag, "users") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def username = column[String]("username", O.Unique)
    def name = column[String]("name")
    def email = column[String]("email", O.Unique)
    def password = column[String]("password")
    def enabled = column[Boolean]("enabled")
    def phone = column[String]("phone")
    def registeredDate = column[DateTime]("registered_date")
    def * : ProvenShape[User] = (id.?, name, email, password, phone, enabled, registeredDate) <> (User.tupled, User.unapply)
  }
}

class UserDAO @Inject()
(protected val dbConfigProvider: DatabaseConfigProvider)
(implicit executionContext: ExecutionContext)
extends UsersComponent with HasDatabaseConfigProvider[JdbcProfile]
{
  private val usersTable = TableQuery[UsersTable]
  private val insertQuery = usersTable returning usersTable.map(_.id) into ((user, id) => user.copy(id = Some(id)))

  def list(page: Int = 0, pageSize: Int = 10): Future[PaginatedResult[User]] = {
    val offset = page * pageSize
    for {
      userSet <- db.run(usersTable.drop(offset).take(pageSize).result)
      userCount <- db.run(usersTable.length.result)
    } yield PaginatedResult(userSet, page, offset, userCount)
  }

  def getById(id: Long): Future[Option[User]] = db.run(usersTable.filter(_.id === id).result.headOption)

  def getByEmail(email: String): Future[Option[User]] = db.run(usersTable.filter(_.email === email).result.headOption)

  def insert(user: User): Future[User] = {
    db.run(insertQuery += user)
  }

  def insert(users: Seq[User]): Future[Seq[User]] = {
    db.run(insertQuery ++= users)
  }

  def update(id: Long, user: User): Future[Unit] = {
    val updatedUser: User = user.copy(Some(id))
    db.run(usersTable.filter(_.id === id).update(updatedUser)).map(_ => ())
  }

  def delete(id: Long): Future[Unit] = db run usersTable.filter(_.id === id).delete.map(_ => ())

}
