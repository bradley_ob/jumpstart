package models.dao

import javax.inject.Inject
import models.{PaginatedResult, Response}
import org.joda.time.DateTime
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.driver.JdbcProfile
import slick.lifted.ProvenShape

import scala.concurrent.{ExecutionContext, Future}


class ResponseDAO @Inject()
(protected val dbConfigProvider: DatabaseConfigProvider)
(implicit executionContext: ExecutionContext)
  extends EventsComponent with HasDatabaseConfigProvider[JdbcProfile]
{
  import profile.api._

  private val responsesTable = TableQuery[ResponseTable]
  private val eventsTable = TableQuery[EventsTable]
  private val insertQuery = responsesTable returning responsesTable.map(_.id) into ((response, id) => response.copy(id = Some(id)))

  private class ResponseTable(tag: Tag) extends Table[Response](tag,"responses") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def eventId = column[Long]("event_id")
    def event = foreignKey("event_fk", eventId, eventsTable)(_.id, onUpdate = ForeignKeyAction.Restrict, onDelete = ForeignKeyAction.Cascade)
    def committed = column[Boolean]("committed")
    def responseDate = column[DateTime]("response_date")
    def description = column[Option[String]]("description")
    def * : ProvenShape[Response] = (id.?, eventId, committed, responseDate, description) <> (Response.tupled, Response.unapply)
  }

  def getById(id: Long): Future[Option[Response]] = db.run(responsesTable.filter(_.id === id).result.headOption)

  def getByUser(userId: Long, page: Int = 0, pageSize: Int = 10): Future[PaginatedResult[Response]] = {
    val offset = page * pageSize
    val responseQuery = for {
      (r, e) <- responsesTable joinLeft eventsTable.filter(_.createdBy === userId) on (_.eventId === _.id)
    } yield r
    // This seems redundant, could perhaps combine into a single construct
    for {
      responseSet <- db.run(responseQuery.drop(offset).take(pageSize).result)
      responseCount <- db.run(responseQuery.length.result)
    } yield PaginatedResult(responseSet, page, offset, responseCount)
  }

  def getByEvent(eventId: Long, page: Int = 0, pageSize: Int = 10): Future[PaginatedResult[Response]] = {
    val offset = page * pageSize
    for {
      eventResponses <- db.run(responsesTable.filter(_.eventId === eventId)
          .drop(offset).take(pageSize)
          .result)
      responseCount <- db.run(responsesTable.filter(_.eventId === eventId).length.result)
    } yield PaginatedResult(eventResponses, page, offset, responseCount)
  }

  def insert(response: Response): Future[Response] = db run (insertQuery += response)
  def insert(responses: Seq[Response]): Future[Seq[Response]] = db run (insertQuery ++= responses)

  def update(id: Long, response: Response): Future[Unit] = {
    val updatedResponse: Response = response.copy(Some(id))
    db.run(responsesTable.filter(_.id === id).update(updatedResponse)).map(_ => ())
  }

  def delete(id: Long): Future[Unit] = db run responsesTable.filter(_.id === id).delete.map(_ => ())
}
