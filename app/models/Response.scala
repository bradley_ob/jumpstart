package models

import org.joda.time.DateTime

case class Response(id: Option[Long], eventId: Long, committed: Boolean, responseDate: DateTime, description: Option[String])
