package models


import org.joda.time.DateTime

case class Event(id: Option[Long], title: String, createdBy: Long, startDate: Option[DateTime],
                 endDate: Option[DateTime], recurrenceType: Option[String], description: Option[String])
