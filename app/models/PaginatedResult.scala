package models

case class PaginatedResult[T] (items: Seq[T], page: Int, offset: Long, total: Long) {
  lazy val next: Option[Int] = Option(page + 1).filter(_ => (offset + items.size) < total)
  lazy val prev: Option[Int] = Option(page - 1).filter(_ >= 0)
}
