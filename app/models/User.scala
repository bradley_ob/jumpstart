package models

import org.joda.time.DateTime

case class User (id: Option[Long],
                 name: String,
                 email: String,
                 password: String,
                 phone: String,
                 enabled: Boolean = true,
                 registeredDate: DateTime = DateTime.now
                )

