package services
import controllers.forms.EventData
import javax.inject.Inject
import models.Event
import models.dao.EventDAO
import org.joda.time.DateTime

import scala.concurrent.ExecutionContext.Implicits._
import scala.concurrent.{ExecutionContext, Future}

class EventService @Inject()(events: EventDAO) {
  def get(id: Long): Future[Option[Event]] = events.getById(id)

  def delete(id: Long): Future[Unit] = events.delete(id)

  def add(eventData: EventData, userId: Long): Future[Event] = {
    val eventReq = Event(
      None,
      eventData.title,
      userId,
      Option(new DateTime(eventData.startDate.getTime)),
      Option(new DateTime(eventData.endDate.getTime)),
      Option(eventData.recurrenceScheme),
      Option(eventData.description)
    )
    for {
      event <- events insert eventReq
    }  yield event
  }

  def update(event: Event): Future[Option[Event]] = {
    events.update(event)
  }

  def update(id: Long, userId: Long, eventData: EventData): Future[Option[Event]] = {
    events.update(
      Event(
        Option(id),
        eventData.title,
        userId,
        Option(new DateTime(eventData.startDate.getTime)),
        Option(new DateTime(eventData.endDate.getTime)),
        Option(eventData.recurrenceScheme),
        Option(eventData.description)
      )
    )
  }

}
