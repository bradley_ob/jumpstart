package services

import play.api.mvc.Session

class UserSessionService {
  val session = new Session()
  lazy val userId: Long = session.get("user") match {
    case Some(id) => id.toLong
    case None => -89
  }
  lazy val userEmail: String = session.get("email") match {
    case Some(email) => email
    case None => "nulled"
  }
}
