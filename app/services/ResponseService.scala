package services

import controllers.forms.ResponseData
import javax.inject.Inject
import models.{PaginatedResult, Response}
import models.dao.ResponseDAO
import org.joda.time.DateTime

import scala.concurrent.ExecutionContext.Implicits._
import scala.concurrent.Future

class ResponseService @Inject()(responses: ResponseDAO) {
  def get(id: Long): Future[Option[Response]] = responses.getById(id)

  def delete(id: Long): Future[Unit] = responses.delete(id)

  def add(responseData: ResponseData, userId: Long, eventId: Long): Future[Response] = {
    val responseReq = Response(
      None,
      eventId,
      responseData.committed,
      new DateTime(),
      Option(responseData.description)
    )
    for {
      response <- responses insert responseReq
    }  yield response
  }

  def getByEvent(eventId: Long, page: Int): Future[PaginatedResult[Response]] =  responses.getByEvent(eventId, page: Int)
  /*
  // TODO: Decide how/if responses can be updated
  def update(response: Response): Future[Option[Response]] = {
    responses.update(response)
  }

  def update(id: Long, userId: Long, eventId: Long, responseData: ResponseData): Future[Option[Response]] = {
    responses.update(
      Response(
        Option(id),
        eventId,
        responseData.committed,
        new DateTime(responseData.responseDate.getTime),
        Option(responseData.description)
      )
    )
  }
  */
}
