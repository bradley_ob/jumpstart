package services
import com.github.t3hnar.bcrypt._
import controllers.forms.{AuthData, UserData}
import javax.inject.Inject
import models.User
import models.dao.UserDAO

import scala.concurrent.ExecutionContext.Implicits._
import scala.concurrent.Future
import scala.util.{Failure, Success}

class UserService @Inject()(users: UserDAO) {
  def login(authData: AuthData): Future[Option[User]] = {
    users.getByEmail(authData.email) map {
      case None => None
      case Some(user) =>
        authData.password isBcryptedSafe user.password match {
          case Success(_) => Some(user)
          case Failure(_) => None
        }
    }
  }

  def add(userData: UserData): Future[Option[User]] = {
    val userReq = User(None, userData.name, userData.email, userData.password.bcrypt, userData.phone)
    for {
      user <- users insert userReq
    } yield Some(user)
  }

  def delete(userId: Long): Future[Unit] = users delete userId

}
